# Associations et collectifs du logiciel libre (et au-delà) à Toulouse

## Associations toulousaines

### Toulibre

Groupe d'utilisateurs et utilisatrices de GNU/Linux et des logiciels libres. Propose ateliers et install parties, et organise chaque année le Capitole du Libre

[toulibre.org](http://toulibre.org/)

### Tetaneutral

Fournisseur d'accès internet et hébergeur associatif sur Toulouse et ses environs, membre de la [fédération FFDN](http://ffdn.org/), aide les gens à monter leur solutions d'accès internet dans leurs communes situées en zone grise ou blanche, propose également de l'hébergement de machines physiques et virtuelles.

[tetaneutral.net](http://www.tetaneutral.net/)

### Tetalab

Hackerspace toulousain, hébergé par et membre du collectif pluridisciplinaire autogéré [Mix'art-Myrys](http://mixart-myrys.org/), coorganisateur du [THSF](https://www.thsf.net/) sur le même lieu, événement festif autour des arts, du hacking et de la bière.

[tetalab.org](https://www.tetalab.org/)

### Le PIC, Projet Internet et Citoyenneté

Hébergeur alternatif et solidaire pour les association. Le PIC s’attache à faire vivre un internet au service des citoyens, et en particulier des associations, afin que l'internet ne soit pas qu'un ensemble de tuyaux utilisés par des sociétés commerciales. 

[le-pic.org](https://www.le-pic.org/)

### Combustible

Association proposant notamment de la médiation numérique pour tous publics

[combustible.fr](http://combustible.fr/)

### Le Culte

[culte.org](https://www.culte.org/)

### LinuxÉdu

Le libre pour l'éducation

[linuxedu.org](https://www.linuxedu.org/)

## Groupes et collectifs locaux

### Groupe local Wikipedia

### Groupe local OpenStreetMap

Depuis septembre 2017 les mappeurs de la région toulousaine se rencontrent régulièrement au  Centre Culturel Bellegarde de 19h00 à 22h00.
[Liste de discussion](http://listes.openstreetmap.fr/wws/info/local-toulouse)
et [page wiki](https://wiki.openstreetmap.org/wiki/FR:Toulouse#Groupe_local_de_Toulouse_et_sa_r.C3.A9gion)

### Oxyta.net

Collectif de dév et d'admin sys proposant des ateliers techniques permettant de créer son propre hébergement

[Oxyta.net](https://oxyta.net/)

### Groupe Media et numérique Alternatiba

Groupe du village AlterMediaTIC, media et numérique, à Alternatiba 2017.

[Alternatiba / Media et Numérique](https://alternatiba.eu/toulouse/alternatiba-2017/thematiques/altermediatic/)

### Les communs toulousains

Réseau des communs à Toulouse, dont les communs de la connaissance et du numérique

[wiki.lescommuns.org/wiki/Toulouse](http://wiki.lescommuns.org/wiki/Toulouse)

### Monnaie libre Occitanie

Collectif ayant pour but de faire connaître le concept de la monnaie libre, et de favoriser l'utilisation de la monnaie libre Ḡ1

[monnaielibreoccitanie.org](http://monnaielibreoccitanie.org/)

### Cryptoparty Toulouse



### Autodéfense numérique

Le collectif d’autodéfense numérique réunit des bénévoles toulousain⋅e⋅s souhaitant partager leurs connaissances en sécurité informatique et usages numériques avec des publics varié⋅e⋅s.

[defensenumerique.licorn.es](https://defensenumerique.licorn.es/)

### Ondecourte.fr

Webradio Toulousaine sur la Culture  du Libre (gérer par des logiciels libres : Radioman...). Deux programmes VHS et Giroll parlent de la culture du Libre et de la culture du hack (stream/podcast).

[ondecourte.fr](http://ondecourte.fr)


## Ailleurs en Occitanie

### Linux Ariège

[linux-ariege.eu.org](https://linux-ariege.eu.org)

### Le Bib

Hackerspace à Montpellier 

[lebib.org](https://lebib.org/)

### ILOTH 

Internet Libre et Ouvert à Tous dans l'Hérault 

[iloth.net](https://iloth.net/)

## Associations nationales

### Framasoft
Framasoft, Réseau dédié à la promotion du «Libre» en général et du logiciel libre en particulier

[framasoft.org](https://framasoft.org/)

### April

### FFDN

La Fédération FDN, Fédération des fournisseurs d’accès à internet associatifs francais

[ffdn.org](https://www.ffdn.org/)

### La Quadrature du Net

La Quadrature du Net, association de défense des droits et libertés fondamentales à l’ère du numérique

[laquadrature.net](https://www.laquadrature.net/)

### Collectif des Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires (C.H.A.T.O.N.S.)

### Wikimedia France

### OpenStreetMap France

### SavoirsCom1

### Centre d'Études sur la Citoyenneté, l'Informatisation et les Libertés

[lececil.org](https://www.lececil.org)

## Sources d'information

[agendadulibre.org/orgas](https://www.agendadulibre.org/orgas)
[carto.framasoft.org](https://carto.framasoft.org) (voir par catégories)

[Source du document](https://framagit.org/altermediatic/alternatives/blob/master/associations-toulouse.md)
